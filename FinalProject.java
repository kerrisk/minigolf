/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;


import java.awt.geom.Line2D;

import static java.lang.Math.abs;
//import javafx.scene.image.PixelReader;//LIBRERIA PARA LEER IMAGEN
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Point2D;
//import javafx.scene.Parent;
import javafx.scene.shape.Line;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.image.*;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.List;
import static javafx.application.Application.launch;
import javafx.scene.text.Font;
//import javafx.scene.layout.Background;

/**
 * @author Almas Baimagambetov (almaslvl@gmail.com)
 */
public class FinalProject extends Application {

    private Pane root;
    private static int golpes =0, level=1, vel_max=2;
    //private static boolean escudo=false;
    //private List<Pelota> bullets = new ArrayList<>();
    //private List<Pelota> enemies = new ArrayList<>();
    //private int vel_max=2;
    //private ImageView nave = new ImageView(new Image(getClass().getResourceAsStream("nave.png")));
    Line line= new Line();
    private Point2D  P1, P2, PR1, PR2;
    private ImageView fondo;
    private double color,color2, X, Y, X_ini, Y_ini, color_der=0.1411764770746231, color_izq=0.20000000298023224, color_arriba=0.05098039284348488, color_abajo=0.0313725508749485, liso=0.800000011920929, zona=1, hoyo=0.49803921580314636,color_subida=0.9686274528503418, color_bajada=0.7411764860153198;
    private boolean colocado = false, iniciar=false,lanzado=false;
    private boolean adentro = true;
    private int [] puntaje= new int[7];
    private int jugadores=1;
    Label lgolpes= new Label("Golpes="+golpes);
    Label nameGame = new Label("Mini Golf");
    Label llevel = new Label("Nivel "+level);
    Button Inicio = new Button();
    Label l1=new Label();
    Label l2=new Label();
    Label l3=new Label();
    Label l4=new Label();
    Label l5=new Label();
    Label l6=new Label();
    Label l7=new Label();
    
    private  double lengthX = 100;
    private double lengthY = 100;

    private Pelota player;

    private Parent createContent() throws Exception{
       
        
        root = new Pane();
        root.getChildren().add(line);
        if(!iniciar){
        fondo=new ImageView(new Image(getClass().getResourceAsStream("Inicio.jpg")));
        Inicio.setText("Press To Start"); 
        Inicio.setVisible(true);
        Inicio.relocate(250, 400);
        root.getChildren().add(fondo);
        root.getChildren().add(Inicio);
        //root.setPrefSize(600, 600);
        añadir_Marcador();
        Inicio.setOnMousePressed(e -> {
            iniciar=true;
            root.getChildren().get(10).setVisible(true);
            //root.getChildren().remove(fondo);
            root.getChildren().remove(Inicio);
            root.setPrefSize(lengthX, lengthY);
            
            cambiar_Nivel(level);
            lgolpes.setVisible(true);
            nameGame.relocate(10, 1);
            lgolpes.relocate(10, 15);
            llevel.relocate(10, 30);
            llevel.setWrapText(true);
            llevel.setVisible(true);      
            line.setVisible(false);
            
            root.getChildren().add(lgolpes);
            root.getChildren().add(nameGame);
            root.getChildren().add(llevel);
            
        });
        
        } 
            //lengthY=fondo.getFitHeight();
            //lengthY=fondo.getFitWidth();
            
            
            
            player = new Player();
            player.setVelocity(new Point2D(0, 0));
            addPelota(player, 300, 30);
            AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                onUpdate();
            }
        };
        timer.start();

        return root;
    }

   /* private void addBullet(Pelota bullet, double x, double y) {
        bullets.add(bullet);
        addPelota(bullet, x, y);
    }

    private void addEnemy(Pelota enemy, double x, double y) {
        //enemy.setVelocity(Point2D.ZERO);
        enemies.add(enemy);
        addPelota(enemy, x, y);
    }
    */

    private void addPelota(Pelota object, double x, double y) {
        object.getView().setTranslateX(x);
        object.getView().setTranslateY(y);
        root.getChildren().add(object.getView());
        root.getChildren().get(10).setVisible(false);
    }

    private void onUpdate() {
        fondo.toBack();
        nameGame.setTextFill(Color.web("#FFFFFF"));
        nameGame.setFont(Font.font("Cambria", 15));
        llevel.setTextFill(Color.web("#FFFFFF"));
        llevel.setFont(Font.font("Cambria", 15));
        lgolpes.setTextFill(Color.web("#FFFFFF"));
        lgolpes.setFont(Font.font("Cambria", 15));
        lgolpes.setText("Golpes: "+String.valueOf(golpes));
        llevel.setText("NIVEL "+String.valueOf(level));
        
       
        //INICIA PRUEBA RGB//----------------------------------------------------------------------
        //_____________________________________________________________________________________
        //_____________________________________________________________________________________
        //_____________________________________________________________________________________
        //System.out.println("COLOR== "+fondo.getImage().getPixelReader().getColor((int)player.getView().getTranslateX(),(int) player.getView().getTranslateY()).getGreen());
        if(player.getView().getTranslateX()<=0){
            player.getView().setTranslateX(0);
        }
        if(player.getView().getTranslateY()<=0){
            player.getView().setTranslateY(0);
        }
        
        color=(fondo.getImage().getPixelReader().getColor((int)player.getView().getTranslateX()+20,(int) player.getView().getTranslateY()).getGreen());
        color2=(fondo.getImage().getPixelReader().getColor((int)player.getView().getTranslateX()-5,(int) player.getView().getTranslateY()+20).getGreen());
        //System.out.println("INCIAR"+iniciar);
        //System.out.println("COLOR= "+(fondo.getImage().getPixelReader().getColor((int)player.getView().getTranslateX(),(int) player.getView().getTranslateY()).getGreen()));
        //System.out.println("X= "+(int) player.getView().getTranslateX()+"\t"+"Y= "+(int) player.getView().getTranslateY()+"COLOR= "+color+"\tADENTRO:"+adentro);
        System.out.println("Color1: "+color+"\tColor2: "+color2);
       adentro=false;
       //FRICCION------------------------------------------------///
       
       if(color==zona||color2==zona){
           adentro=true;
           if(lanzado){
               System.out.println("ZONA.....");
               System.out.println("X:"+X+"Y"+Y);
               //friccion(.02,.02);
               friccion(((-1)*X_ini)*.01,(-Y_ini)*.01);
               System.out.println("X:"+X+"Y"+Y);
//player.setVelocity(player.getVelocity().add(-(player.getVelocity().getX()/50), -(player.getVelocity().getY()/50)));
           }       
       }else if((color==liso||color2==liso)&&lanzado){
           //System.out.println("PISO LISO......");
           friccion(((-1)*X_ini)*.01,(-Y_ini)*.01);
           //friccion(.02,.02);
       }else if((color==hoyo||color2==hoyo)&&lanzado){
           while(level<=8){
            resset();
            break;
           }
           
       }else if((color==color_bajada||color2==color_bajada)&&lanzado){
           //System.out.println("PISO LISO......");
           friccion_pendiente(0,.04, -1);
           //friccion(.02,.02);
       }else if((color==color_subida||color2==color_subida)&&lanzado){
           //System.out.println("PISO LISO......");
           friccion_pendiente(0,.04, 1);
           //friccion(.02,.02);
       }
            
        if(lanzado){ 
            line.setVisible(false);
        }
        //Comportamiento de la Pelota
        System.out.println("X="+X+" Y="+Y);
        //System.out.println("COLOR= "+color);
        if ((color == color_arriba||color2 == color_abajo || color2 == color_arriba||color == color_abajo)&&lanzado) {
            //System.out.println("ADENTROOOO");
            System.out.println("Y= "+player.getVelocity().getY());
            Y=Y*(-1);
            Y_ini=Y_ini*(-1);
            //player.setVelocity(new Point2D(player.getVelocity().getX(),player.getVelocity().getY()*(-1)));
            System.out.println("Y DESPUES= "+player.getVelocity().getY());
        }
        //System.out.println("Lanzado"+lanzado);
        if ((color == color_der	||color2 == color_izq || color2 == color_der	||color == color_izq)&&lanzado) {
              X=X*(-1);  
              X_ini=X_ini*(-1);
            //player.setVelocity(new Point2D(player.getVelocity().getX()*(-1),player.getVelocity().getY()));
        }   
        if(lanzado){
            player.setVelocity(new Point2D(X,Y));
        }
        if(player.getVelocity().magnitude()==0&&(X==0||X==-0)&&(Y==0||Y==-0)&&lanzado){
            System.out.println("X="+X+" Y="+Y);
            //System.out.println("FALSE-> MOVIENDOSE");
             //colocado=false;
            System.out.println("LANZADO FALSE");
            lanzado=false;
        }
        
        
        //System.out.println("VELOCIDAD= "+player.getVelocity().magnitude());
        player.update();

    }
    private void añadir_Marcador(){
        l1.setVisible(false);
        root.getChildren().add(l1);
        l2.setVisible(false);
        root.getChildren().add(l2);
        l3.setVisible(false);
        root.getChildren().add(l3);
        l4.setVisible(false);
        root.getChildren().add(l4);
        l5.setVisible(false);
        root.getChildren().add(l5);
        l6.setVisible(false);
        root.getChildren().add(l6);
        l7.setVisible(false);
        root.getChildren().add(l7);
        l1.relocate(10, 50);
        l2.relocate(10, 70);
        l3.relocate(10, 90);
        l4.relocate(10, 110);
        l5.relocate(10, 130);
        l6.relocate(10, 150);
        l7.relocate(10, 170);
        l1.setTextFill(Color.web("#FFFFFF"));
        l1.setFont(Font.font("Cambria", 15));
        l2.setTextFill(Color.web("#FFFFFF"));
        l2.setFont(Font.font("Cambria", 15));
        l3.setTextFill(Color.web("#FFFFFF"));
        l3.setFont(Font.font("Cambria", 15));
        l4.setTextFill(Color.web("#FFFFFF"));
        l4.setFont(Font.font("Cambria", 15));
        l5.setTextFill(Color.web("#FFFFFF"));
        l5.setFont(Font.font("Cambria", 15));
        l6.setTextFill(Color.web("#FFFFFF"));
        l6.setFont(Font.font("Cambria", 15));
        l7.setTextFill(Color.web("#FFFFFF"));
        l7.setFont(Font.font("Cambria", 15));
        
        
    }
    private void cambiar_Nivel(int n){
        root.getChildren().remove(fondo);
        switch(n){
               case 1:
                    fondo=new ImageView(new Image(getClass().getResourceAsStream("Level9.bmp")));
                    root.getChildren().add(fondo);
                    break;
                case 2:
                    fondo=new ImageView(new Image(getClass().getResourceAsStream("Level2.bmp")));
                    root.getChildren().add(fondo);
                    l1.setVisible(true);
                    l1.setText("P1= "+String.valueOf(puntaje[level-2]));
                    break;
                case 3:
                    fondo=new ImageView(new Image(getClass().getResourceAsStream("Level3.bmp")));
                    root.getChildren().add(fondo);
                    l2.setVisible(true);
                    l2.setText("P2= "+String.valueOf(puntaje[level-2]));
                    break;
               case 4:
                    fondo=new ImageView(new Image(getClass().getResourceAsStream("Level4.bmp")));
                    root.getChildren().add(fondo);
                    l3.setVisible(true);
                    l3.setText("P3= "+String.valueOf(puntaje[level-2]));
                    break;
               case 5:
                    fondo=new ImageView(new Image(getClass().getResourceAsStream("Level5.bmp")));
                    root.getChildren().add(fondo);
                    l4.setVisible(true);
                    l4.setText("P4= "+String.valueOf(puntaje[level-2]));
                    break;
               case 6:
                    fondo=new ImageView(new Image(getClass().getResourceAsStream("Level6.bmp")));
                    root.getChildren().add(fondo);
                    l5.setVisible(true);
                    l5.setText("P5= "+String.valueOf(puntaje[level-2]));
                    break;
               case 7:
                    fondo=new ImageView(new Image(getClass().getResourceAsStream("Level7.bmp")));
                    root.getChildren().add(fondo);
                    l6.setVisible(true);
                    l6.setText("P6= "+String.valueOf(puntaje[level-2]));
                    break;
               case 8:
                    fondo=new ImageView(new Image(getClass().getResourceAsStream("Fin.bmp")));
                    root.getChildren().add(fondo);
                    l7.setVisible(true);
                    l7.setText("P7= "+String.valueOf(puntaje[level-1]));
                    setPosition();
                    break;
               
               
                    
            }
    }
    private void setPosition(){
        l1.relocate(250, 50);
        l2.relocate(250, 70);
        l3.relocate(250, 90);
        l4.relocate(250, 110);
        l5.relocate(250, 130);
        l6.relocate(250, 150);
        l7.relocate(250, 170);
    }
    private void friccion(double x2, double y2){
        System.out.println("FRICCION");
        if(X!=0){
            if(X>0){
              X+=x2; 
              if(X<=0){
                  X=0;
              }
            }else{
                X+=x2;
                if(X>=0){
                  X=0;
              }
            }
        }
        if(Y!=0){
            if(Y>0){
              Y+=y2;
              if(Y<=0){
                  Y=0;
              }
            }else{
                Y+=y2;
                if(Y>=0){
                  Y=0;
              }
            }
        }
        
        verificar();
        
    }
    private void friccion_pendiente(double x2, double y2, int p){
        System.out.println("FRICCION");
        if(X!=0){
            if(X>0){
              X+=x2; 
              if(X<=0){
                  X=0;
              }
            }else{
                X+=x2;
                if(X>=0){
                  X=0;
              }
            }
        }
        if(p==-1){
            Y-=y2;
        }else if(p==1){
            Y+=y2;
            
        }
        
        verificar_pendiente();
        
    }
    
    private void resset(){
        puntaje[level-1]=golpes;
        
        level++;
        golpes=0;
        colocado=false;
        lanzado=false;
        player.getView().setTranslateX(250);
        player.getView().setTranslateY(500);
        X=0;
        Y=0;
        player.setVelocity(new Point2D(X,Y));
        cambiar_Nivel(level);
        
        
    }
    private void verificar(){
        //System.out.println("VERIFICAR");
        if(X_ini<0&&X>0){
            X=0;
            
        }else if(X_ini>0&&X<0){
            X=0;
        }
        if(Y_ini<0&&Y>0){
            Y=0;
            
        }else if(Y_ini>0&&Y<0){
            Y=0;
        }
    }
    private void verificar_pendiente(){
        //System.out.println("VERIFICAR");
        if(X_ini<0&&X>0){
            X=0;
            
        }else if(X_ini>0&&X<0){
            X=0;
        }
    }

    private static class Player extends Pelota {

        Player() {
            
            super(new Circle(10, 10, 10,Color.WHITE),20);
        }
    }
    
    

    @Override
    public void start(Stage stage) throws Exception {
        
        stage.setScene(new Scene(createContent()));
        stage.getScene().setOnMouseMoved(e -> {
            if(!colocado&&!lanzado){
                System.out.println("Mouse Moved");
                player.getView().setTranslateX(e.getX());
                player.getView().setTranslateY(e.getY());
            }
            
        });
        stage.getScene().setOnMouseClicked(e -> {
            //System.out.println("LANZADO="+lanzado);
            if((adentro||colocado)&&(!lanzado)){
                
                player.getView().setTranslateX(e.getX());
                player.getView().setTranslateY(e.getY());
                //System.out.println("EX: "+e.getX()+"EY: "+e.getY());
                
                
                colocado=true;
                System.out.println("COLOCADO="+colocado);
            }
        });
        
        
        
        stage.getScene().setOnMouseDragged(e ->{
            //System.out.println("Arrastre: X:"+e.getX()+" Y:"+e.getY());
            X=(player.getView().getTranslateX()-e.getX())/20;
            Y=(player.getView().getTranslateY()-e.getY())/20;
            if(abs(X)>10){
                if(X<0){X=-10;}
                else{X=10;}
            }
            if(abs(Y)>10){
                if(Y<0){Y=-10;}
                else{Y=10;}
            }
            X_ini=X;
            Y_ini=Y;
            //Modificamos Linea de Tiro
            if(!lanzado){
                line.setVisible(true);
                line.setStartX(player.getView().getTranslateX()+10);
                line.setStartY(player.getView().getTranslateY()+10);
                line.setEndX(player.getView().getTranslateX()-(e.getX()-player.getView().getTranslateX()));
                line.setEndY(player.getView().getTranslateY()-(e.getY()-player.getView().getTranslateY()));
            }else{
                line.setVisible(false);
                
            }//Modificamos Velocidad
            //System.out.println("X:"+X+" Y:"+Y);
           // System.out.println("TranslateX: "+player.getView().getTranslateX()+"Translate y: "+player.getView().getTranslateY());
            
        });
        stage.getScene().setOnMouseReleased(e ->{
            if(colocado&&!lanzado){
                System.out.println("REALESSSEEED");
                player.setVelocity(player.getVelocity().add(X, Y));
                golpes++;
                System.out.println("LANZADO=TRUE");
                lanzado=true;
                
            }
        });
        stage.show();
    }

    public static void main(String[] args) {
        
        launch(args);
    }
}
