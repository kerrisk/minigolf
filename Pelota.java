/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finalproject;
import javafx.geometry.Point2D;
import javafx.scene.Node;

/**
 * @author Almas Baimagambetov (almaslvl@gmail.com)
 */
public class Pelota {

    private int radio=0;
    private Node view;
    private Point2D velocity = new Point2D(1, 1);

    private boolean alive = true;

    public Pelota(Node view) {
        this.view = view;
    }
    
    public Pelota(Node view, int radio) {
        this.view = view;
        this.radio = radio;
    }
    public void getRadious(){
        ;
    }

    public void update() {
        view.setTranslateX(view.getTranslateX() + velocity.getX());
        view.setTranslateY(view.getTranslateY() + velocity.getY());
    }

    public void setVelocity(Point2D velocity) {
        this.velocity = velocity;
    }
  
    

    public Point2D getVelocity() {
        return velocity;
    }

    public Node getView() {
        return view;
    }

    public void resetView() {
        
        this.view.setTranslateX(300);
        this.view.setTranslateY(300);
        this.setVelocity(new Point2D(0,0));
    }
    

    public boolean isAlive() {
        return alive;
    }

    public boolean isDead() {
        return !alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public double getRotate() {
        return view.getRotate();
    }

    public void rotateRight() {
        view.setRotate(view.getRotate() + 5);
        setVelocity(new Point2D(Math.cos(Math.toRadians(getRotate())), Math.sin(Math.toRadians(getRotate()))));
    }

    public void rotateLeft() {
        view.setRotate(view.getRotate() - 5);
        setVelocity(new Point2D(Math.cos(Math.toRadians(getRotate())), Math.sin(Math.toRadians(getRotate()))));
    }

    public boolean isColliding(Pelota other) {
        return getView().getBoundsInParent().intersects(other.getView().getBoundsInParent());
    }
    
    public int getRadio() {
        return radio;
    }
}
